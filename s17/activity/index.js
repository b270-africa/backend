// console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userDetails(){
	let fullName = prompt("What is your Full name?"); 
	let age = prompt("How old are you? "); 
	let location = prompt("Where do you live? ");

	console.log("Hello, " + fullName); 
	console.log("You are " + age + " " + "years old"); 
	console.log("You live in " + location); 
};

userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function bandNames() {
			
			let bandName1 = "1. The Beatles";
			let bandName2 = "2. Metallica";
			let bandName3 = "3. The Eagles";
			let bandName4 = "4. L'arc~en~Ciel";
			let bandName5 = "5. Eraserheads";

			console.log(bandName1);
			console.log(bandName2);
			console.log(bandName3);
			console.log(bandName4);
			console.log(bandName5);
		};

		bandNames();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function topFiveMovies() {
			
			let movie1 = "1. The GodFather";
			let movie2 = "2. The GodFather, Part II";
			let movie3 = "3. Shawshank Redemption";
			let movie4 = "4. To kill A Mockingbird";
			let movie5 = "5. Psycho";

			console.log(movie1);
			console.log("Rotten Tomatoes Rating: 97%");
			console.log(movie2);
			console.log("Rotten Tomatoes Rating: 96%");
			console.log(movie3);
			console.log("Rotten Tomatoes Rating: 91%");
			console.log(movie4);
			console.log("Rotten Tomatoes Rating: 93%");
			console.log(movie5);
			console.log("Rotten Tomatoes Rating: 96%");
		};

		topFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);