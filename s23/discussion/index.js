// console.log("Hello World");

// [SECTION] Objects
/*
	- An object is a data type that is used to represent real world objects
	- Information is stored and represented in a "key-value" pair
*/

// Creating objects using initializers/literal notation
/*
	- This creates/declares an object and also initializes/assigns its properties upon creating
	- A cellphone is an example of a real world object
	- It has its own properties such as name, color, price, weight, unit, etc.
	- Syntax:
		let objectname = {
			keyA: valueA,
			keyB: valueB,
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/literal notation:");
console.log(cellphone);
console.log(typeof cellphone);


// Creating objects using a constructor function

/*
	- Creates a reusable function to create several objects that have the same data structure
	- An "instance" is a concrete occurence of any object which emphasizes on the distinct/unique idnetity of it
	- Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA,
			this.keyB = keyB
		}
*/

//  The "this" keyword allows us to assign a new object's property by associating them with the values received from a constructor fuction's parameters
function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// This is a unique instance of the Laptop object
// The "new" operator creates an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructors: ");
console.log(laptop);

// This is a another unique instance of the Laptop object
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using object constructors: ");
console.log(myLaptop);

// This only invokes/calls the "Laptop" function instead of creating a new object instance
let oldLaptop = Laptop("Acer", 1980);
console.log("Result from creating objects without using the new keyword: ");
console.log(oldLaptop); //undefined

// Creating empty objects
let computer = {};
console.log(computer);

let myComputer = new Object();
console.log(myComputer);



// [SECTION] Accessing Object properties

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name); //Macbook Air

// Using the square notation
console.log("Result from square backet notation: " + myLaptop["name"]);


// [SECTION] Initializing/Adding/Deleting/Reassigning Object properties

let car = {};
console.log(car);

// Initializing/adding object properties using dot notation
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

// Initializing/adding object properties using square bracket notation
car["manufactureDate"] = 2019;
console.log("Result from adding properties using square bracket notation: ");
console.log(car);


// Reassigning object properties
car.name = "Ford Raptor";
console.log("Result from reassigning properties: ")
console.log(car)

// Deleting object properties
delete car["manufactureDate"];
console.log("Result from deleting properties: ");
console.log(car);


// [SECTION] Object Methods
/*
	- A method is a function property of an object
	- method are useful for creating objetc specific function which are used to perform tasks on them
	- Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it shoudl work
*/
let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person);
console.log("Result from object methods: ");
person.talk();

// Adding methods objects
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.")
}

person.walk();
console.log(person);


let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com", "joesmith@email.xyz"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();
// Output: Hello! My name is Joe Smith


/*
	Scenario:
	1. We would like to create a game that would have several pokemon interac with each other
	2. Every pokemon would have the same set of stats, properties, and functions
*/

// Create multiple kinds of pokemon
// Using object literals to create multiple pokemons would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonhealth");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);

// Object constructor

function Pokemon(name, level) {

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + "tackled" + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonhealth");
	};
	this.faint = function(){
		console.log(this.name + "fainted.");
	}
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 8);
console.log(rattata);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create an interaction between the two pokemon/objects
pikachu.tackle(rattata);

rattata.tackle(pikachu);