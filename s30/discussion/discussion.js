db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: [ "Philippines", "US"]
	},

	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: [ "Philippines", "Ecuador"]
	},	

	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: [ "US", "China"]
	},	

	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: [ "Philippines", "India"]
	},	
]);

// [ SECTION ] MongoDB Aggregation

	// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.

// [ SUB-SECTION ] Using the Aggregate Method

	// then $match is used to pass the coduments that meet the specified conditions(s) to the next pipeline stage/aggregation process.

		// Syntax
			//  { $match: { field: value }}

		// Syntax
			//  { $group: { _id: "value", fieldResult: "valueResult"} }
			//  Using the $match and $group along with aggregation will find for products that are on sale and will group the total amount of stock for suppliers found.

		// Syntax
			// db.collectionName.aggregate([
				// { $match: {fieldA : valueA }},
				// { $group: { _id: "$fieldB"}, { results: { operations } }} 
			// ])

		//  The symbol $ will refer to a field name that is available in the documents that are being aggregated on.

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock" } }}
]);

// [ SUB-SECTION ] Field Projection with Aggregation

	// The $project can be used when aggregating data to include/exclude fields from the returned results

		// Syntax:
			// { $project : { field: 1/0 } }

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock" } }},
	{ $project : { _id: 0 } }
]);

// [ SUB-SECTION ] Sorting Aggregated Results

	// The $sort can be used to change the order of aggregated results
	//  Providing a value of -1 will sort the aggregated results in a reverse order

		// Syntax:
			//  { $sort : {_id: 1/-1 }}

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock" } }},
	{ $sort : { total: -1 } }
]);

// [ SUB-SECTION ] Aggregating Results Based on Array Fields

	// The $unwind deconstructs an array field from a collection/field with an array value to output a result for each element.

	// Syntax:
		// { $unwind: "field" }

db.fruits.aggregate([
	{ $unwind: "$origin" }
]);

// [ SUB-SECTION ] Display Fruits Documents by their Origin and the Kind of Fruits that are Supplied

db.fruits.aggregate([
	{ $unwind: "$origin" },
	{ $group: { _id: "$origin", kinds : { $sum : 0} }}
]);