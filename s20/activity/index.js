// console.log("Hellow World");

// 3.
let number = Number(prompt("Give me a number."));



console.log("The number you provided is: " + number);

// 4.		
for(i = number; i > 0; i--) {

		// 5.
		if(i <= 50) {
        	console.log("The current value is at " + i + ". Terminating the loop");
		break;
		}

		// 6.
		if (i % 10 === 0){
            console.log ("The number is divisible by 10; Skipping the number");
            continue;
        }

		// 7. 
        if (i % 5 === 0){
            console.log (i);
            continue;
        }

}

// 8.
let characters = "supercalifragilisticexpialidocious";
console.log (characters);

// 9. 
let emptyVowels ="";

// 10. 
for (let i = 0; i < characters.length; i++){

	// 11.
    if (characters[i] === 'a' ||
        characters[i] === 'e' ||
        characters[i] === 'i' ||
        characters[i] === 'o' ||
        characters[i] === 'u'        ) {
        continue;

	// 12. 
    } else {
        emptyVowels += characters[i] ;
    }
}   
console.log (emptyVowels);

