//CRUD Operations
/*
	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

// [SECTION] Inserting documents (CREATE)
/*
	Syntax:
		db.collectionName.insertOne({object})

		Insert Many:
		db.collectionName.insertMany([{objectA}, {objectB}])
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	course: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many
db.users.insertMany([

	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "0987654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "0987654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	},
])


// [SECTION] Finding documents (READ/RETRIEVE)
/*
	- Syntax:
		db.collectionName.find();
		db.collectionName.find({ field: value});
*/

// Leaving the search criteria empty will retrieve ALL of the documents
db.users.find();

// Finding a single document
db.users.find({ firstName: "Stephen"});
db.users.find({ age: 82});

// Finding documents with multiple parameters
/*
	- Syntax: 
		db.collectionName.find({fieldA: valueA, fieldB: valueB});
*/
db.users.find({ lastName: "Armstrong", age: 82});
db.users.find({ firstName: "Jane", age: 21});


// [SECTION] Updating documents (UPDATE)

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})


//Updating a single document
/*
	- Just like the "find" method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/
db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName : "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)
db.users.find({ firstName: "Bill"});

// Updating one field
db.users.updateOne(
	{ firstName: "Test"},
	{
		$set: {firstName: "MJ"}
	}
)

// Updating multiple documents
/*
	- Syntax:
		db.collectionName. updateMany({criteria}, {$set: {field: value}})
*/

db.users.updateMany(
	{"department": "none"},
	{
		$set: {department: "HR"}
	}
)

// Replace One
/*
	- Can be used if replacing the whole document is necessary
*/
db.users.replaceOne(
	{ firstName: "Bill"},
	{
		firstName: "Bill",
		lastName : "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
)

db.users.replaceOne(
	{firstName: "MJ"},
	{
		lastName: "Africa",
		age: 12,
		department: "Operations"
	}
)

// Deleting documents (DELETE)

// Document to delete
db.users.insertOne({
	firstName: "test"
})

// Deleting a single document
/*
	-Syntax
		db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "test"});


// Delete Many
/*
	- Be careful when using the "deleteMany". If no search criteria is provided, it will delete all the documents in a database
*/
db.users.deleteMany({firstName: "Bill"});



// [SECTION] Advanced Queries

// Query on embedded document
db.users.find({
	contact: {
		phone: "0987654321",
		email: "stephenhawking@gmail.com"
	}
})

db.users.find({
	"contact.email": "janedoe@gmail.com"
})

// Querying an array with exact elements (In order)
db.users.find({
	course: ["CSS", "JavaScript", "Python"]
})

// Querying an array without regard to order
db.users.find({
	courses: {$all: ["React", "Python"]}
})


// Querying an embedded array

db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
})

db.users.find({
	namearr: 
		{
		namea: "juan"
		}
})