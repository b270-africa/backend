const http = require("http");

// Creates a variable "port" to store the port number
const port = 4000

// Creates a variable "server" that stores output of the "createServer()" method.
const server = http.createServer((request, response) => {

	// Accessing the 'greeting' route returns a message of "Hello World!"
	if (request.url == '/greeting') {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello, Bact 270!")

	// Accessing the '/homepage' route returns a message of "This is the homepage"
	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("This is the homepage")

	// Mini activity
	// Create another endpoint for the "/profile" and send a response "This is the profile page".
		
	} else if (request.url == '/profile') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("This is the profile page")

	// All other routes will return a message of "Page not available"
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("Page not available")
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}.`);