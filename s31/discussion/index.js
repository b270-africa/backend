// Use the "require" directive to load Node.js modules
//  A module is a software component or part of the program that contains one or more routines/functions
// "http" is a Node JS module that lets Node JS transfer data using Hyper Text Transfer Protocol
// Provides a functionality for creating HTTP servers and clients, allowing applications to send and receive HTTP requests
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client

// The messages sent by the client, usually a web browser, are called "requests".
// The messages sent by the server as answers are called responses

// The server will be assigned to port 4000 via "listen(4000)" method where the server will listen to any requests that are sent to it
//  A port is a virtual point where network connection start and end
http.createServer(function (request, response) {

	// Used the writeHead() method to:
		//  set the status code for the response - 200 mean OK
		// Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with the text content "Hello World!"
	response.end("Hello World!")

}).listen(4000);

// When server is running, console will print the message:
console.log("Server is running at localhost:4000");