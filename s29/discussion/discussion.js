// Comparison Query Operator

// [SUB-SECTION] $gt/gte Operator

	// Allows us to find doduments that have field number values greater than or equal to a specific value.

		// Syntax:
			// db.collectionName.find({ field: { $gt: value } });
			// db.collectionName.find({ field: { $gte: value } });


db.users.find({ age : { $gt : 50 } });
db.users.find({ age : { $gte : 50 } });

// [ SUB-SECTION ] $lt/$lte Operator
	
	// Allows us to find documents that have field number values less than or equal to a specified value.

		// Syntax:
			// db.collectionName.find ({ field: {$lt: value}})
			// db.collectionName.find ({ field: {$lte: value}})

db.users.find({ age : { $lt : 50 } });
db.users.find({ age : { $lte : 50 } });

// [ SUB-SECTION ] $ne Operator

	// Allows us to find document that have field number values not equal to a specified value.

		// Syntax:
			// db.collectionName.find ({ field: { $ne: value} });

db.users.find({ age : {$ne: 82 } });

// [ SUB-SECTION ] $in Operator
	
	// Allows us to find documents with specific match criteria one field using different values

		// Syntax:
			// db.collectionName.field({ field: { $in : value} });

db.users.find({ lastName: { $in : ["Hawking", "Doe"]} });
db.users.find({ courses: { $in : ["HTML", "React"]} });

// [ SECTION ] Logical Query Operators

// [ SUB-SECTION ] $or Operator

	// Allows us to find documents that match a single criteria from multiple provided search criteria.

		// Syntax:
			// db.collectionName.find({ $or: [ { fieldA : valueA }, { fieldB : valueB }] })

db.users.find( { $or: [{ firstName: "Neil" }, { age: 25 }] });
db.users.find( { $or: [{ firstName: "Neil" }, { age: { $gt: 30 } }] });

// [ SUB-SECTION ] $and Operator

	// Allows us to find documents matching criteria is a single filed

		// Syntax:
			// db.collectionName.find({ $and: [{ fieldA: { valeA } }, { fieldB: { valeB } }] });

db.users.find({ $and: [{ age: { $lt: 90} }, { age: { $gt: 80 } }] });

// [ SECTION ] Field Projection

	// Retrieving documents are common operations that we do and by default MongoDB queries return the whodle document as a response.
	// When dealing with complex data structures, there might be instances when field are not useful for the query that we are trying to accomplish.

// [ SUB-SECTION ] Inclusion
	
	// Allows us to include/ add specific fields only when retrieving documents.
	// The values provided is 1 to denote that the field is being included.
		
		// Syntax:
			// db.collectionName.find({ criteria}, {field1: 1});

db.users.find({ firstName: "Jane"}, { firstName: 1, lastName: 1, contact: 1 });

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// [ SUB-SECTION ] Exclusion

	// Allows us to exclude/remove specific field only when retrieving documents.
	// The provided is ) to denote that the field is being included.

		// Syntax:
			// db.collectionName.find({ criteria }, {field: 0});

db.users.find({ firstName: "Jane" }, { contact: 0, department: 0});

// [ SUB-SECTION ] Suppressing the ID field

	// Allows us to exclude the "_id" field when retrieving documents.

		// Syntax:
			// db.collectionName.find({  criteria }, { _id:0 });

db.users.find({ firstName: "Jane"}, {firstName: 1, lastName: 1, contact: 1, _id: 0 });

// [ SUB-SECTION ] Returning Specific Fields in Embedded Documents

db.users.find({ firstName: "Jane"}, { firstName: 1, lastName: 1, "contact.phone": 1});

// [ SUB_SECTION ] Supressing Specific Fields in Embedded Documents

db.users.find({ firstName: "Jane"}, { "contact.phone": 0});

// [ SECTION ] Evaluation Query Operators

// [ SUB-SECTION ] $regex Operator

	// Allows us to find documents that match a specific string or pattern using regular expressions.

	// Syntax:
		// db.collectionName.find({ field: $regex : 'pattern', $options: '$optionValue'});

// Case Sensitive Query
db.users.find({ firstName: { $regex: 'N'} });

// Case Insensitive Query
db.users.find({ firstName: { $regex: 'j', $options: 'i'} });