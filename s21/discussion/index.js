// console.log("Hello World")

// [SECTION] Arrays
//  An array in programming simply a list of data.
//  They are declared using the square brackets ([]) also known as "Array Literals".

let studentNumberA = "2023-1923";
let studentNumberB = "2023-1924";
let studentNumberC = "2023-1925";
let studentNumberD = "2023-1926";
let studentNumberE = "2023-1927";

let studentNumbers = ["2023-1923", "2023-1924", "2023-1925", "2023-1926", "2023-1927"];
console.log(studentNumbers);

// Common examples of arrays
let grades = [71, 100, 85, 90];
console.log(grades);

let computerBrands = ["Acer", "Lenovo", "Dell", "Asus", "Apple", "Huawei"];
console.log(computerBrands);

// Possible use of an array but is not recommended
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr);

// Alternative way to write arrays
let myTasks = [
	"drink",
	"eat javascript",
	"inhale css",
	"bake bootstrap"
	];
console.log(myTasks);

// Creating an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Nairobi";
let city4 = "Rio";

let cities = [city1, city2, city3, city4];
console.log(cities);


// [SECTION] .length property

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also be used with strings to count the number of characters. Spaces are also included.
let fullName = "Mark Joseph Africa";
console.log(fullName.length);

// length property can also set the total number of items in an array by simply updating the length property of the array
					// 4-1
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// Another example using decrementation
cities.length--;
console.log(cities);

// Will not work with string
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);//Mark Joseph Africa

// If we can shorten an array by setting the length property, we can also lengthen it by adding a number into the length property. however, it will be empty.
let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);
theBeatles.length++;
console.log(theBeatles);

// [SECTION] Reading fromArrays

console.log(grades[0]);//71
console.log(computerBrands[3]);//Asus

// Accessing an array element that does not exist will return "undefined"
console.log(grades[20]);//undefined

let lakersLegends = ["Kobe", "Lebron", "Shaq", "Magic", "Kareem"];

console.log(lakersLegends[3]); //Magic
console.log(lakersLegends[1]); //Lebron
console.log(lakersLegends[2]); //Shaq

// You can also store array items inside another variable
let currentLaker = lakersLegends[1];
console.log(currentLaker);

// You can also reassign array values using element's index
console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Davis";
console.log("Array after reassignment");
console.log(lakersLegends);


// Accessing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
						// 5-1 = 4
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]); //Kukoc
						// 5-1 = 4
console.log(bullsLegends[bullsLegends.length-1]); //Kukoc

// Adding items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

console.log(newArr.length); //2

// Adding an item at the end of the array
// newArr[1] = "Barrett Wallace" (newArr.length-1)
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// Looping over an array
						// index < 3
for (let index = 0; index < newArr.length; index++) {

	console.log(newArr[index]);
}


let numArr = [5, 12, 30, 46, 40];

// Checks if each element in the array is divisible by 5 or not.
for (let index = 0; index < numArr.length; index++) {

	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}
}

// [SECTION] Multi-dimentional Arrays
// Multidimentional arrays are useful for storing complex data structures

let chestBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h2"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h2"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];
console.table(chestBoard);
					//[row][column]
console.log(chestBoard[1][4]); //e2
console.log(chestBoard[7][7]); //h8
console.log(chestBoard[4][1]); //b5
console.log(chestBoard[2][6]); //g3

console.log("Pawn moves to: " + chestBoard[1][5]); //f2
