const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	},
	description : {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	slots: {
		type: Number,
		required: [true, "Slots is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,

		// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			isPaid: {
				type: Boolean,
				default: true
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}
	]
})
module.exports = mongoose.model("Course", courseSchema);