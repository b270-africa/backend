// ACTIVITY
/*
1. Create a User model with the following properties:
- firstName - String
- lastName - String
- email -String
- password - String
- isAdmin - Boolean
- mobileNo - String
- enrollments - Array of objects
    - courseId - String
    - enrolledOn - Date (Default value - new Date object)
    - status - String (Default value - Enrolled)
2. Make sure that all the fields are required to ensure consistency in the user information being stored in our database.
3. Create a git repository named S37-S41.
4. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.
*/

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName : {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
				type: Boolean,
				default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	createdOn: {
			type: Date,
			default: new Date()
	},
	// The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})
module.exports = mongoose.model("User", userSchema);